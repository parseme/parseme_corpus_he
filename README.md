README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Hebrew, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/home) initiative for the full documentation of the annotation principles.
The present Hebrew data result from an update of the Hebrew part of the [PARSEME 1.2 corpus](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3367).
For the changes with respect to the 1.2 version, see the change log below.

Corpora
-------
All the annotated data come from one of these sources:
1. News and articles from the Arutz 7 news website, 2001-2006, collected by the [MILA Knowledge Center for Processing Hebrew](http://www.mila.cs.technion.ac.il/)
2. News and articles from the HaAretz news website, 1990-1991, collected by the [MILA Knowledge Center for Processing Hebrew](http://www.mila.cs.technion.ac.il/)
3. Articles from the TheMarker financial newspaper, May - October 2002, collected by the 7[MILA Knowledge Center for Processing Hebrew](http://www.mila.cs.technion.ac.il/)


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe).
* UPOS (column 4): Available. Automatically annotated (UDPipe).
* XPOS (column 5): Available. Automatically annotated (UDPipe).
* FEATS (column 6): Available. Automatically annotated (UDPipe).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated (UDPipe).
* DEPS (column 8): Available. Automatically annotated (UDPipe).
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: LVC.full, LVC.cause, VID. 
The automatically performed annotations stem from [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) run with the `hebrew-iahltwiki-ud-2.10-220711` model.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Tokenization
------------
The data is tokenized by the generic corpus tokenizer, which can be downloaded from the PARSEME shared task web page.


Known issues
-------------
Information about lemmas is missing for some sentences.  The missing lemmas are represented by the underscore `_` in the `LEMMA` column.


Authors
-------
Language Leader: Chaya Liebeskind
The annotation team consists of 4 members:  Hevi Elyovich, Ruth Malka, Rinat Walles and Chaya Liebeskind


License
-------
The data are distributed under the terms of the [CC-BY-NC-SA v4](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

Contact
-------
liebchaya@gmail.com

Reference
---------
When using this corpus, please cite:

@InProceedings{Liebeskind:2016,
  author = {Chaya Liebeskind and Yaakov HaCohen-Kerner},
  title = {A Lexical Resource of Hebrew Verb-Noun Multi-Word Expressions},
  booktitle = {Proceedings of the Tenth International Conference on Language Resources and Evaluation},
  series  = {LREC'16},
  pages = {522--527},
  year = {2016},
  month = {may},
  date = {23-28},
  address = {Portoroz, Slovenia},
  publisher = {European Language Resources Association (ELRA)},
 }

@article{hebrew-resources:2008,
  author = {Itai, Alon and Wintner, Shuly},
  journal = {Language Resources and Evaluation},
  month = {March},
  number = {1},
  pages = {75-98},
  title = {Language resources for {H}ebrew},
  volume = {42},
  year = {2008}
}

Change Log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
      - updating the morphosyntactic annotation using [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) and the `hebrew-iahltwiki-ud-2.10-220711` model.
      - correcting errors identified by consistency checks

* 2020-07-09:

  - [Version 1.2](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3367) of the corpus was released on LINDAT.
  - Changes with respect to the 1.1 version are the following:
    - adding new annotated files (TheMarker corpus), 500 sentences.
    - providing a companion raw corpus, automatically annotated for morpho-syntax (HaAretz and TheMarker corpora)
      - UDPipe annotation relied on the model `hebrew-ud-2.0-170801.udpipe`

* 2018-04-30:

  - [Version 1.1](https://lindat.mff.cuni.cz/repository/xmlui/handle/11372/LRT-2842) of the corpus was released on LINDAT.
  - Changes with respect to the 1.0 version are the following:
    - updating the existing VMWE annotations to comply with PARSEME [guidelines edition 1.1](https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.1/).

* 2017-01-20:

  - [Version 1.0](https://lindat.mff.cuni.cz/repository/xmlui/handle/11372/LRT-2282) of the corpus was released on LINDAT.
  - All the annotated data came from one of these sources:
    1. News and articles from the Arutz 7 news website, 2001-2006, collected by the [MILA Knowledge Center for Processing Hebrew](http://www.mila.cs.technion.ac.il/)
    2. News and articles from the HaAretz news website, 1990-1991, collected by the [MILA Knowledge Center for Processing Hebrew](http://www.mila.cs.technion.ac.il/)
  - The data were adapted in the following way:
    - adding the verbal multiword expression annotation layer, according to the PARSEME shared task guidelines version 1.0.
    - transforming them into the parseme-tsv format (CoNLL-like)
    - adding the automatically generated layer of syntactic dependencies (see below for details)
